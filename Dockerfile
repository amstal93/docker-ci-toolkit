ARG IMAGE_FROM_SHA
# hadolint ignore=DL3006
FROM ${IMAGE_FROM_SHA}

ARG IMAGE_FROM_SHA
ARG RELEASE_VERSION
ARG BUILD_DATE
ARG VCS_REF="DEV"

ENV container docker

LABEL maintainer="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.title="ci-toolkit" \
    org.opencontainers.image.description="A lightweight Docker image to easily build Docker images" \
    org.opencontainers.image.authors="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.licenses="GPL-3.0-or-later" \
    org.opencontainers.image.version="${RELEASE_VERSION}" \
    org.opencontainers.image.url="https://cloud.docker.com/repository/docker/jfxs/ci-toolkit" \
    org.opencontainers.image.source="https://gitlab.com/fxs/docker-ci-toolkit" \
    org.opencontainers.image.revision=${VCS_REF} \
    org.opencontainers.image.created=${BUILD_DATE}

COPY files /usr/local/bin/

# hadolint ignore=DL3018
RUN apk --no-cache add \
        ca-certificates \
        curl \
        git \
        jq \
        make && \
    /usr/local/bin/get-local-versions.sh -f ${IMAGE_FROM_SHA} -c docker -s ci-toolkit=${RELEASE_VERSION} && \
    mkdir "$HOME/.docker" && \
    printf "{\n  \"experimental\": \"enabled\"\n}" > "$HOME/.docker/config.json"

# hadolint ignore=DL3059
RUN addgroup nobody root && \
    mkdir -p /app && \
    chgrp -R 0 /app && \
    chmod -R g=u /etc/passwd /app

WORKDIR /app

#!/bin/sh
#
# get-tag-index.sh is a shell program to get the first available index for a Docker tag.
#
# Copyright (c) 2019 FX Soubirou
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# get-tag-index.sh uses the Open Source/Free Software "curl" by Daniel Stenberg
# distributed under the MIT/X derivate license:
# https://curl.haxx.se/docs/copyright.html

VERSION=__VERSION__

HUB_ENDPOINT="https://hub.docker.com/v2"

REPO_NAME=""
USERNAME=""
PASSWORD=""
TAG_VERSION=""
MANDATORY_ARGUMENT=1

usage() {
    echo "This script gets the first available index for a Docker tag on Dockerhub."
    echo "Tag should have version-index format. Index starts at 1."
    echo ""
    echo "Usage: get-tag-index.sh -r <repository> -u <username> -p <password> -t <tag-version>"
    echo "curl and jq are mandatory"
    echo ""
    echo "Options:"
    echo "  -r Repository"
    echo "  -u Username of repository admin account"
    echo "  -p Password of repository admin account"
    echo "  -t Tag"
    echo "  -v | --version print version"
    echo "  -h print usage"
    echo ""
    echo "Example:  2.8.0-1 and 2.8.0-2 are existing tags"
    echo "get-tag-index.sh -r jfxs/ansible -u dockerhubUser -p dockerhubPassword -t 2.8.0 returns 3"
}

version() {
    year=$(date +'%Y')
    echo "$0, version $VERSION"
    echo "Copyright (C) $year FX Soubirou"
    echo "License GPLv3+ : GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>"
    echo "get-tag-index.sh uses the Open Source/Free Software \"curl\" by Daniel Stenberg"
    echo "distributed under the MIT/X derivate license:"
    echo "https://curl.haxx.se/docs/copyright.html"
}

test_argument() {
    if [ -z "$2" ]; then
        echo "Error: -$1 argument is mandatory !" >&2
        MANDATORY_ARGUMENT=0
    fi
}

if [ "$1" = "--version" ]; then
    version
    exit 0
fi

while getopts hp:r:t:u:v option; do
    case "${option}" in
    h)
        usage
        exit 0
        ;;
    v)
        version
        exit 0
        ;;
    r)
        REPO_NAME="${OPTARG}"
        ;;
    u)
        USERNAME="${OPTARG}"
        ;;
    p)
        PASSWORD="${OPTARG}"
        ;;
    t)
        TAG_VERSION="${OPTARG}"
        ;;
    *)
        usage
        exit 1
        ;;
    esac
done

test_argument "r" "$REPO_NAME"
test_argument "u" "$USERNAME"
test_argument "p" "$PASSWORD"
test_argument "t" "$TAG_VERSION"

if [ "$MANDATORY_ARGUMENT" = "0" ]; then
    usage
    exit 1
fi

# Get auth token
token_response=$(curl -w "%{http_code}" -s -H "Content-Type: application/json" -X POST -d "{\"username\": \"${USERNAME}\", \"password\": \"${PASSWORD}\"}" "${HUB_ENDPOINT}"/users/login/)
http_code=$(echo "$token_response" | tr -d '\n' | tail -c 3)
if [ "${http_code}" != "200" ]; then
    printf "Unable to login to Dockerhub, response code: %s\\n" "${http_code}" >&2
    exit 1
fi
token=$(echo "$token_response" | tr -d '\n' | sed "s/^\\({.*}\\).*/\\1/g" | jq -r .token)

# Get tags list from Dockerhub
tags_response=$(curl -w "%{http_code}" -s -H "Authorization: JWT ${token}" "${HUB_ENDPOINT}"/repositories/"${REPO_NAME}"/tags/?page_size=10000)
http_code=$(echo "$tags_response" | tr -d '\n' | tail -c 3)
if [ "${http_code}" != "200" ]; then
    printf "Unable to get tags list for %s on Dockerhub, response code: %s\\n" "${REPO_NAME}" "${http_code}" >&2
    exit 1
fi
tags=$(echo "$tags_response" | tr -d '\n' | sed "s/^\\({.*}\\).*/\\1/g" | jq -r '.results|.[]|.name')

index=0
for tag in $tags
do
    if echo "$tag" | grep "^$TAG_VERSION-*" >/dev/null; then
        tag_index=$(echo "$tag" | sed 's/^.*-\([0-9]*\)/\1/')
        if [ "$tag_index" -gt "$index" ] 2>/dev/null; then
            index=$tag_index;
        fi
    fi
done

index=$((index + 1))
echo "$index"

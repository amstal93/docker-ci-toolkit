#!/bin/sh
#
# image-to-badge.sh is a shell program to generate a badge from a Docker image
# output.xml file.
#
# Copyright (c) 2020 FX Soubirou
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

VERSION=__VERSION__

IMAGE=""
BADGE_DIR=$(pwd)
MANDATORY_ARGUMENT=1

usage() {
    echo "This script generates a badge for size and layers count of a Docker image."
    echo ""
    echo "Usage: image-to-badge.sh -i <image> [-d <badges_directory>]"
    echo ""
    echo "Options:"
    echo "  -i docker image"
    echo "  -d badges directory, default current directory"
    echo "  -v | --version print version"
    echo "  -h print usage"
    echo ""
    echo "Example:"
    echo "image-to-badge.sh -i jfxs/ci-toolkit:latest -d public"
}

version() {
    year=$(date +'%Y')
    echo "$0, version $VERSION"
    echo "Copyright (C) $year FX Soubirou"
    echo "License GPLv3+ : GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>"
}

test_argument() {
    if [ -z "$2" ]; then
        echo "Error: -$1 argument is mandatory !" >&2
        MANDATORY_ARGUMENT=0
    fi
}

if [ "$1" = "--version" ]; then
    version
    exit 0
fi

# Add space between size and unit
format_size() {
    length_size=$(printf "%s" "$1" | wc -m | tr -d '[:space:]')
    length_size_1=$((length_size - 1))
    unit=$(printf "%s" "$1" | tail -c 1)
    size=$(printf "%s" "$1" | cut -c1-$length_size_1)
    size_rounded=$(echo "($size+0.5)/1" | bc)
    size_formated=$(printf "%s %sB" "$size_rounded" "$unit")

    echo "$size_formated"
}

while getopts d:hi:v option; do
    case "${option}" in
    h)
        usage
        exit 0
        ;;
    v)
        version
        exit 0
        ;;
    i)
        IMAGE="${OPTARG}"
        ;;
    d)
        BADGE_DIR="${OPTARG}"
        ;;
    *)
        usage
        exit 1
        ;;
    esac
done

test_argument "i" "$IMAGE"

if [ "$MANDATORY_ARGUMENT" = "0" ]; then
    usage
    exit 1
fi

if ! [ -x "$(command -v docker)" ]; then
    echo "docker command not found" >&2
    exit 1
fi

if ! docker pull "$IMAGE"; then
    echo "Impossible to pull image: $IMAGE" >&2
    exit 1
fi

mkdir -p "$BADGE_DIR"
docker save "$IMAGE" > "$BADGE_DIR"/image-size.tar
gzip -f "$BADGE_DIR"/image-size.tar
size=$(find "$BADGE_DIR" -maxdepth 1 -name image-size.tar.gz -exec ls -lh {} \; | awk -F ' ' '{print $5}')
rm -f image-size.tar.gz

size_formated=$(format_size "$size")
layer=$(docker history -q "$IMAGE" | wc -l | tr -d '[:space:]')
echo "Image: $IMAGE | size: $size_formated | layers: $layer"

BADGE_BEG='<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="129" height="20"><linearGradient id="b" x2="0" y2="100%"><stop offset="0" stop-color="#bbb" stop-opacity=".1"/><stop offset="1" stop-opacity=".1"/></linearGradient><clipPath id="a"><rect width="129" height="20" rx="3" fill="#fff"/></clipPath><g clip-path="url(#a)"><path fill="#555" d="M0 0h68v20H0z"/><path fill="#1488c6" d="M68 0h61v20H68z"/><path fill="url(#b)" d="M0 0h129v20H0z"/></g><g fill="#fff" text-anchor="middle" font-family="DejaVu Sans,Verdana,Geneva,sans-serif" font-size="110">'
BADGE_IMG='<image x="5" y="3" width="14" height="14" xlink:href="data:image/svg+xml;base64,PHN2ZyBmaWxsPSIjMTQ4OEM2IiByb2xlPSJpbWciIHZpZXdCb3g9IjAgMCAyNCAyNCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48dGl0bGU+RG9ja2VyIGljb248L3RpdGxlPjxwYXRoIGQ9Ik00LjgyIDE3LjI3NWMtLjY4NCAwLTEuMzA0LS41Ni0xLjMwNC0xLjI0cy41Ni0xLjI0MyAxLjMwNS0xLjI0M2MuNzQ4IDAgMS4zMS41NiAxLjMxIDEuMjQycy0uNjIyIDEuMjQtMS4zMDUgMS4yNHptMTYuMDEyLTYuNzYzYy0uMTM1LS45OTItLjc1LTEuOC0xLjU2LTIuNDJsLS4zMTUtLjI1LS4yNTQuMzFjLS40OTQuNTYtLjY5IDEuNTUzLS42MyAyLjI5NS4wNi41NjIuMjQgMS4xMi41NTQgMS41NTQtLjI1NC4xMy0uNTY4LjI1LS44MS4zNzctLjU3LjE4Ny0xLjEyNC4yNS0xLjY4LjI1SC4wOTdsLS4wNi4zN2MtLjEyIDEuMTgyLjA2IDIuNDIuNTYyIDMuNTRsLjI0NC40MzV2LjA2YzEuNSAyLjQ4MyA0LjE3IDMuNiA3LjA3OCAzLjYgNS41OTQgMCAxMC4xODItMi40MiAxMi4zNTctNy42MzMgMS40MjUuMDYyIDIuODY0LS4zMSAzLjU0LTEuNjc2bC4xOC0uMzEtLjMtLjE4N2MtLjgxLS40OTQtMS45Mi0uNTYtMi44NS0uMzFsLS4wMTguMDAyem0tOC4wMDgtLjk5MmgtMi40Mjh2Mi40MmgyLjQzVjkuNTE4bC0uMDAyLjAwM3ptMC0zLjA0M2gtMi40Mjh2Mi40MmgyLjQzVjYuNDhsLS4wMDItLjAwM3ptMC0zLjEwNGgtMi40Mjh2Mi40MmgyLjQzdi0yLjQyaC0uMDAyem0yLjk3IDYuMTQ3SDEzLjM4djIuNDJoMi40MlY5LjUxOGwtLjAwNy4wMDN6bS04Ljk5OCAwSDQuMzgzdjIuNDJoMi40MjJWOS41MThsLS4wMS4wMDN6bTMuMDMgMGgtMi40djIuNDJIOS44NFY5LjUxOGwtLjAxNS4wMDN6bS02LjAzIDBIMS40djIuNDJoMi40MjhWOS41MThsLS4wMy4wMDN6bTYuMDMtMy4wNDNoLTIuNHYyLjQySDkuODRWNi40OGwtLjAxNS0uMDAzem0tMy4wNDUgMEg0LjM4N3YyLjQySDYuOFY2LjQ4bC0uMDE2LS4wMDN6Ii8+PC9zdmc+"/>'
BADGE_END=" <text x=\"435\" y=\"150\" fill=\"#010101\" fill-opacity=\".3\" transform=\"scale(.1)\" textLength=\"410\">${size_formated}</text><text x=\"435\" y=\"140\" transform=\"scale(.1)\" textLength=\"410\">${size_formated}</text><text x=\"975\" y=\"150\" fill=\"#010101\" fill-opacity=\".3\" transform=\"scale(.1)\" textLength=\"510\">${layer} layers</text><text x=\"975\" y=\"140\" transform=\"scale(.1)\" textLength=\"510\">${layer} layers</text></g> </svg>"

echo "$BADGE_BEG$BADGE_IMG$BADGE_END"  > "$BADGE_DIR"/docker.svg

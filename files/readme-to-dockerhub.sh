#!/bin/sh
#
# readme-to-dockerhub.sh is a shell program to update full description of a Dockerhub repository.
#
# Copyright (c) 2019 FX Soubirou
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# readme-to-dockerhub.sh uses the Open Source/Free Software "curl" by Daniel Stenberg
# distributed under the MIT/X derivate license:
# https://curl.haxx.se/docs/copyright.html

VERSION=__VERSION__

HUB_ENDPOINT="https://hub.docker.com/v2"
REPO_NAME=""
USERNAME=""
PASSWORD=""
README_FILE=""
MANDATORY_ARGUMENT=1

usage() {
    echo "This script updates full description of a Dockerhub repository."
    echo ""
    echo "Usage: readme-to-dockerhub.sh -r <repository> -u <username> -p <password> -f <file_path>"
    echo "curl and jq are mandatory"
    echo ""
    echo "Options:"
    echo "  -r Repository"
    echo "  -u Username of repository admin account"
    echo "  -p Password of repository admin account"
    echo "  -f file path of the readme file"
    echo "  -v | --version print version"
    echo "  -h print usage"
    echo ""
    echo "Example:"
    echo "readme-to-dockerhub.sh -r jfxs/ci-toolkit -u dockerhubUser -p dockerhubPassword -f README.md"
}

version() {
    year=$(date +'%Y')
    echo "$0, version $VERSION"
    echo "Copyright (C) $year FX Soubirou"
    echo "License GPLv3+ : GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>"
    echo "readme-to-dockerhub.sh uses the Open Source/Free Software \"curl\" by Daniel Stenberg"
    echo "distributed under the MIT/X derivate license:"
    echo "https://curl.haxx.se/docs/copyright.html"
}

test_argument() {
    if [ -z "$2" ]; then
        echo "Error: -$1 argument is mandatory !" >&2
        MANDATORY_ARGUMENT=0
    fi
}

if [ "$1" = "--version" ]; then
    version
    exit 0
fi

while getopts f:hp:r:u:v option; do
    case "${option}" in
    h)
        usage
        exit 0
        ;;
    v)
        version
        exit 0
        ;;
    r)
        REPO_NAME="${OPTARG}"
        ;;
    u)
        USERNAME="${OPTARG}"
        ;;
    p)
        PASSWORD="${OPTARG}"
        ;;
    f)
        README_FILE="${OPTARG}"
        ;;
    *)
        usage
        exit 1
        ;;
    esac
done

test_argument "r" "$REPO_NAME"
test_argument "u" "$USERNAME"
test_argument "p" "$PASSWORD"
test_argument "f" "$README_FILE"

if [ "$MANDATORY_ARGUMENT" = "0" ]; then
    usage
    exit 1
fi

if ! [ -e "$README_FILE" ]; then
    echo "Error: readme file $README_FILE does not exist!" >&2
    exit 1
fi

content=$(cat "$README_FILE")
json=$(jq -n --arg desc "$content" '{"full_description": $desc }')

# Get auth token
token_response=$(curl -w "%{http_code}" -s -H "Content-Type: application/json" -X POST -d "{\"username\": \"${USERNAME}\", \"password\": \"${PASSWORD}\"}" "${HUB_ENDPOINT}"/users/login/)
http_code=$(echo "$token_response" | tr -d '\n' | tail -c 3)
if [ "${http_code}" != "200" ]; then
    printf "Unable to login to Dockerhub, response code: %s\\n" "${http_code}" >&2
    exit 1
fi
token=$(echo "$token_response" | tr -d '\n' | sed "s/^\\({.*}\\).*/\\1/g" | jq -r .token)

# Set full description
desc_response=$(curl -w "%{http_code}" -s -d "$json" -X PATCH -H "Content-Type: application/json" -H "Authorization: JWT ${token}" "${HUB_ENDPOINT}"/repositories/"${REPO_NAME}"/)
http_code=$(echo "$desc_response" | tr -d '\n' | tail -c 3)
if [ "${http_code}" != "200" ]; then
    printf "Unable to set full description for %s on Dockerhub, response code: %s\\n" "${REPO_NAME}" "${http_code}" >&2
    exit 1
fi

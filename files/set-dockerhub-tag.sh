#!/bin/sh
#
# set-dockerhub-tag.sh is a shell program to publish images on Dockerhub with specific tags.
#
# Copyright (c) 2019 FX Soubirou
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

VERSION=__VERSION__

USERNAME=""
PASSWORD=""
SERVER="docker.io"
IMAGE=""
REPOSITORY=""
TAG=""
LATEST=0
MAJOR=0
MINOR=0
MANDATORY_ARGUMENT=1

usage() {
    echo "Usage: set-dockerhub-tag.sh -u <username> -p <password> [-s <server>] -i <source-image> -r <image-repository> -t <tag> [-l] [-M] [-m]"
    echo ""
    echo "Options:"
    echo "  -u username of repository admin account"
    echo "  -p password of repository admin account"
    echo "  -s registry server. By default: docker.io (dockerhub)"
    echo "  -i source image"
    echo "  -r image repository"
    echo "  -t tag"
    echo "  -l add tag latest"
    echo "  -M add major version tag: 3.2.1-4 -> tag 3"
    echo "  -m add minor version tag: 3.2.1-4 -> tag 3.2"
    echo "  -v | --version print version"
    echo "  -h print usage"
    echo ""
    echo "Example with images built and pushed on gitlab.com registry:"
    echo "set-dockerhub-tag.sh -u dockerhubUser -p dockerhubPassword \\"
    echo "-i registry.gitlab.com/fxs/docker-hello-world/master-amd64:499694f5 -r jfxs/hello-world -t 1.19.3-2 -l -M -m"
}

version() {
    year=$(date +'%Y')
    echo "$0, version $VERSION"
    echo "Copyright (C) $year FX Soubirou"
    echo "License GPLv3+ : GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>"
}

test_argument() {
    if [ -z "$2" ]; then
        echo "Error: -$1 argument is mandatory !" >&2
        MANDATORY_ARGUMENT=0
    fi
}

if [ "$1" = "--version" ]; then
    version
    exit 0
fi

while getopts hi:lMmp:r:s:t:u:v option; do
    case "${option}" in
    h)
        usage
        exit 0
        ;;
    v)
        version
        exit 0
        ;;
    u)
        USERNAME="${OPTARG}"
        ;;
    p)
        PASSWORD="${OPTARG}"
        ;;
    s)
        SERVER="${OPTARG}"
        ;;
    i)
        IMAGE="${OPTARG}"
        ;;
    r)
        REPOSITORY="${OPTARG}"
        ;;
    t)
        TAG="${OPTARG}"
        ;;
    l)
        LATEST=1
        ;;
    M)
        MAJOR=1
        ;;
    m)
        MINOR=1
        ;;
    *)
        usage
        exit 1
        ;;
    esac
done

test_argument "u" "$USERNAME"
test_argument "p" "$PASSWORD"
test_argument "i" "$IMAGE"
test_argument "r" "$REPOSITORY"
test_argument "t" "$TAG"

if [ "$MANDATORY_ARGUMENT" = "0" ]; then
    usage
    exit 1
fi

if ! [ -x "$(command -v docker)" ]; then
    echo "docker command not found" >&2
    exit 1
fi

if ! docker pull "$IMAGE"; then
    echo "Impossible to pull source image: $IMAGE" >&2
    exit 1
fi

if ! docker tag "$IMAGE" "$SERVER/$REPOSITORY:$TAG"; then
    echo "Impossible to tag image" >&2
    exit 1
fi

if ! docker login -u "$USERNAME" -p "$PASSWORD" "$SERVER"; then
    echo "Impossible to login to $SERVER" >&2
    exit 1
fi

if ! docker push "$SERVER/$REPOSITORY:$TAG"; then
    echo "Impossible to push image" >&2
    exit 1
fi

if [ "$LATEST" = 1 ]; then
    docker tag "$IMAGE" "$SERVER/$REPOSITORY:latest"
    docker push "$SERVER/$REPOSITORY:latest"
fi

if [ "$MAJOR" = 1 ]; then
    major_version=$(echo "$TAG" | sed 's/[^0-9.]*\([0-9]*\).*/\1/')
    if [ -z "$major_version" ]; then
        echo "Invalid tag for major version" >&2
        exit 1
    else
        docker tag "$IMAGE" "$SERVER/$REPOSITORY:$major_version"
        docker push "$SERVER/$REPOSITORY:$major_version"
    fi
fi

if [ "$MINOR" = 1 ]; then
    minor_version=$(echo "$TAG" | sed 's/[^0-9.]*\([0-9]*.[0-9]*\).*/\1/')
    if [ -z "$minor_version" ]; then
        echo "Invalid tag for minor version" >&2
        exit 1
    else
        docker tag "$IMAGE" "$SERVER/$REPOSITORY:$minor_version"
        docker push "$SERVER/$REPOSITORY:$minor_version"
    fi
fi

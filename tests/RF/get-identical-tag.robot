** Settings ***
Documentation     get-identical-tag.sh tests
...
...               A test suite to validate get-identical-tag shell script.

Library           OperatingSystem
Library           Process
Library           String

*** Variables ***
${TESTS_DIR}   tests
${SHELL_DIR}   files
${SHELL.SH}   get-identical-tag.sh
${USAGE_DISPLAY}   Usage: ${SHELL.SH} -r
${REPOSITORY}   myRepository
${USERNAME}   Username
${PASSWORD}   password
${ARCH}   amd64
${TAG}   latest

*** Test Cases ***
with no option
    [Tags]    get-identical-tag    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -r argument is mandatory
    And Should Contain    ${result.stderr}    -u argument is mandatory
    And Should Contain    ${result.stderr}    -p argument is mandatory
    And Should Contain    ${result.stderr}    -t argument is mandatory
    And Should Contain    ${result.stderr}    -a argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

with unknown option
    [Tags]    get-identical-tag    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -z
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Illegal option -z
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-r] option missing
    [Tags]    get-identical-tag    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -u   ${USERNAME}   -p   ${PASSWORD}   -t   ${TAG}   -a   ${ARCH}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -r argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-r] option with missing argument
    [Tags]    get-identical-tag    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   -u   ${USERNAME}   -p   ${PASSWORD}   -t   ${TAG}   -a   ${ARCH}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-u] option missing
    [Tags]    get-identical-tag    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -p   ${PASSWORD}   -t   ${TAG}   -a   ${ARCH}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -u argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-u] option with missing argument
    [Tags]    get-identical-tag    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   -p   ${PASSWORD}   -t   ${TAG}   -a   ${ARCH}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-p] option missing
    [Tags]    get-identical-tag    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -t   ${TAG}   -a   ${ARCH}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -p argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-p] option with missing argument
    [Tags]    get-identical-tag    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -p   -t   ${TAG}   -a   ${ARCH}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-t] option missing
    [Tags]    get-identical-tag    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -p   ${PASSWORD}   -a   ${ARCH}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -t argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-t] option with missing argument
    [Tags]    get-identical-tag    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -p   ${PASSWORD}   -t   -a   ${ARCH}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-a] option missing
    [Tags]    get-identical-tag    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -p   ${PASSWORD}   -t   ${TAG}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -a argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-a] option with missing argument
    [Tags]    get-identical-tag    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -p   ${PASSWORD}   -t   ${TAG}   -a
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-x] option with missing argument
    [Tags]    get-identical-tag    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -p   ${PASSWORD}   -t   ${TAG}   -a   ${ARCH}   -x
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-v] option
    [Tags]    get-identical-tag    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -v
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${SHELL.SH}, version

[--version] option
    [Tags]    get-identical-tag    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   --version
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${SHELL.SH}, version

[-h] option
    [Tags]    get-identical-tag    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -h
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

Wrong option username
    [Tags]    get-identical-tag    dockerhub
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   wrongUsername   -p   ${PASSWORD}   -t   ${TAG}   -a   ${ARCH}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Unable to login to Dockerhub, response code: 401

Wrong option password
    [Tags]    get-identical-tag    dockerhub
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -p   wrongPassword   -t   ${TAG}   -a   ${ARCH}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Unable to login to Dockerhub, response code: 401

Wrong option repository
    [Tags]    get-identical-tag    dockerhub
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   wrongRepository   -u   ${USERNAME}   -p   ${PASSWORD}   -t   ${TAG}   -a   ${ARCH}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Unable to get tags list for wrongRepository on Dockerhub, response code: 404

Wrong tag option
    [Tags]    get-identical-tag    dockerhub
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -p   ${PASSWORD}   -t   wrongTag   -a   ${ARCH}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Error: Tag wrongTag not found for architecture ${ARCH}

Wrong architecture option
    [Tags]    get-identical-tag    dockerhub
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -p   ${PASSWORD}   -t   ${TAG}   -a   wrongArch
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Error: Tag ${TAG} not found for architecture wrongArch

Wrong regex option
    [Tags]    get-identical-tag    dockerhub
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -p   ${PASSWORD}   -t   ${TAG}   -a   ${ARCH}   -x   \\\([0-9]*.[0-9]*.[0-9]\\\)-wrong
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Error: Regex

Successful request for amd64 architecture
    [Tags]    get-identical-tag    dockerhub    sanity
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -p   ${PASSWORD}   -t   ${TAG}   -a   amd64
    Then Should Be Equal As Integers    ${result.rc}    0
    @{result_list} =    And Split String    ${result.stdout}
    ${length_list} =    And Get Length    ${result_list}
    And Should Be Equal As Integers    ${length_list}    2
    ${length_sha} =    And Get Length    ${result_list}[0]
    And Should Be Equal As Integers    ${length_sha}    71
    And Should Contain    ${result_list}[0]    sha256:
    And Should Contain    ${result_list}[1]    latest
    And Should Contain    ${result_list}[1]    3.2.1-1,3.2,3

Successful request with regex for amd64 architecture
    [Tags]    get-identical-tag    dockerhub
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -p   ${PASSWORD}   -t   ${TAG}   -a   amd64   -x   \\\([0-9]*.[0-9]*.[0-9]\\\)-[0-9]*
    Then Should Be Equal As Integers    ${result.rc}    0
    @{result_list} =    And Split String    ${result.stdout}
    ${length_list} =    And Get Length    ${result_list}
    And Should Be Equal As Integers    ${length_list}    3
    ${length_sha} =    And Get Length    ${result_list}[0]
    And Should Be Equal As Integers    ${length_sha}    71
    And Should Contain    ${result_list}[0]    sha256:
    And Should Contain    ${result_list}[1]    latest
    And Should Contain    ${result_list}[1]    3.2.1-1,3.2,3
    And Should Contain    ${result_list}[2]    3.2.1

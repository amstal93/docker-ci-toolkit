** Settings ***
Documentation     get-local-versions.sh tests
...
...               A test suite to validate get-local-versions shell script.

Library           OperatingSystem
Library           Process
Library           String

*** Variables ***
${TESTS_DIR}   tests
${SHELL_DIR}   files
${SHELL.SH}   get-local-versions.sh
${USAGE_DISPLAY}   Usage: ${SHELL.SH} -f
${DOCKER_IMAGE_DIGEST}   jfxs/ansible@sha256:cad01fac1d46947561aaea97e75d20ae1f3e5f53f6ae7baa5d879e1d25207928
${VERSIONS_FILE}   /etc/VERSIONS
${ALPINE_VERSION_FILE}   /etc/alpine-release
${ALPINE_VERSION_PATTERN}   \\d+.\\d+.\\d+
${ALPINE_PACKAGE_PATTERN}   \\d+.\\d+.\\d+-r\\d+
${PYTHON_PACKAGE_PATTERN}   \\d+.\\d+.{0,1}\\d*
${PACKAGE1_VERSION}   package1=1.0.0
${PACKAGE2_VERSION}   package2=2.0.0

*** Test Cases ***
with no option
    [Tags]    get-local-versions    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -f argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

with unknown option
    [Tags]    get-local-versions    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -x
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Illegal option -x
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-v] option
    [Tags]    get-local-versions    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -v
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${SHELL.SH}, version

[--version] option
    [Tags]    get-local-versions    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   --version
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${SHELL.SH}, version

[-h] option
    [Tags]    get-local-versions    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -h
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-f] only argument
    [Tags]    get-local-versions    root    sanity
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${DOCKER_IMAGE_DIGEST}
    Then Should Be Equal As Integers    ${result.rc}    0
    And VERSIONS file should contain x lines and should match from and alpine version   2

[-f] option with missing argument
    [Tags]    get-local-versions    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-a] with one alpine package argument
    [Tags]    get-local-versions    root
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${DOCKER_IMAGE_DIGEST}   -a   busybox
    Then Should Be Equal As Integers    ${result.rc}    0
    @{lines} =   And VERSIONS file should contain x lines and should match from and alpine version   3
    And Line should match alpine package version   ${lines[2]}   busybox

[-a] with multiple alpine packages argument
    [Tags]    get-local-versions    root
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${DOCKER_IMAGE_DIGEST}   -a   busybox,alpine-baselayout
    Then Should Be Equal As Integers    ${result.rc}    0
    @{lines} =   And VERSIONS file should contain x lines and should match from and alpine version   4
    And Line should match alpine package version   ${lines[2]}   busybox
    And Line should match alpine package version   ${lines[3]}   alpine-baselayout

[-a] option with missing argument
    [Tags]    get-local-versions    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${DOCKER_IMAGE_DIGEST}   -a
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-p] with one python package argument
    [Tags]    get-local-versions    root
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${DOCKER_IMAGE_DIGEST}   -p   robotframework
    Then Should Be Equal As Integers    ${result.rc}    0
    @{lines} =   And VERSIONS file should contain x lines and should match from and alpine version   3
    And Line should match python package version   ${lines[2]}   robotframework

[-p] with multiple python packages argument
    [Tags]    get-local-versions    root
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${DOCKER_IMAGE_DIGEST}   -p   robotframework,robotframework-requests
    Then Should Be Equal As Integers    ${result.rc}    0
    @{lines} =   And VERSIONS file should contain x lines and should match from and alpine version   4
    And Line should match python package version   ${lines[2]}   robotframework
    And Line should match python package version   ${lines[3]}   robotframework-requests

[-p] option with missing argument
    [Tags]    get-local-versions    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${DOCKER_IMAGE_DIGEST}   -p
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-c] with one cli program argument
    [Tags]    get-local-versions    root
    ${version} =   Get python package version   robotframework
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${DOCKER_IMAGE_DIGEST}   -c   robot
    Then Should Be Equal As Integers    ${result.rc}    0
    @{lines} =   And VERSIONS file should contain x lines and should match from and alpine version   3
    And Should Be Equal   ${lines[2]}   robot=${version}|C

[-c] with multiple cli program argument
    [Tags]    get-local-versions    root
    ${version_rf} =   Get python package version   robotframework
    ${version_pip} =   Get python package version   pip
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${DOCKER_IMAGE_DIGEST}   -c   robot,pip
    Then Should Be Equal As Integers    ${result.rc}    0
    @{lines} =   And VERSIONS file should contain x lines and should match from and alpine version   4
    And Should Be Equal   ${lines[2]}   robot=${version_rf}|C
    And Should Be Equal   ${lines[3]}   pip=${version_pip}|C

[-c] with one wrong cli program argument
    [Tags]    get-local-versions    root
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${DOCKER_IMAGE_DIGEST}   -c   wrongProgram
    Then Should Be Equal As Integers    ${result.rc}    0
    @{lines} =   And VERSIONS file should contain x lines and should match from and alpine version   3
    And Should Be Equal   ${lines[2]}   wrongProgram=ERROR_CMD_NOT_FOUND|C

[-c] with a cli program argument with a space
    [Tags]    get-local-versions    root
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${DOCKER_IMAGE_DIGEST}   -c   pip -v
    Then Should Be Equal As Integers    ${result.rc}    0
    @{lines} =   And VERSIONS file should contain x lines and should match from and alpine version   3
    And Should Be Equal   ${lines[2]}   pip -v=ERROR_CMD_NOT_FOUND|C

[-c] option with missing argument
    [Tags]    get-local-versions    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${DOCKER_IMAGE_DIGEST}   -c
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-s] with one custom package argument
    [Tags]    get-local-versions    root
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${DOCKER_IMAGE_DIGEST}   -s   ${PACKAGE1_VERSION}
    Then Should Be Equal As Integers    ${result.rc}    0
    @{lines} =   And VERSIONS file should contain x lines and should match from and alpine version   3
    And Should Be Equal   ${lines[2]}   ${PACKAGE1_VERSION}|S

[-s] with multiple custom packages argument
    [Tags]    get-local-versions    root
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${DOCKER_IMAGE_DIGEST}   -s   ${PACKAGE1_VERSION},${PACKAGE2_VERSION}
    Then Should Be Equal As Integers    ${result.rc}    0
    @{lines} =   And VERSIONS file should contain x lines and should match from and alpine version   4
    And Should Be Equal   ${lines[2]}   ${PACKAGE1_VERSION}|S
    And Should Be Equal   ${lines[3]}   ${PACKAGE2_VERSION}|S

[-s] option with missing argument
    [Tags]    get-local-versions    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${DOCKER_IMAGE_DIGEST}   -s
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-a] with multiple alpine packages argument, [-p] with multiple python packages argument,[-c] with multiple cli program argument and [-s] with multiple specific packages argument
    [Tags]    get-local-versions    root
    ${version_rf} =   Get python package version   robotframework
    ${version_pip} =   Get python package version   pip
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${DOCKER_IMAGE_DIGEST}   -a   busybox,alpine-baselayout   -p   robotframework,robotframework-requests   -c   robot,pip   -s   ${PACKAGE1_VERSION},${PACKAGE2_VERSION}
    Then Should Be Equal As Integers    ${result.rc}    0
    @{lines} =   And VERSIONS file should contain x lines and should match from and alpine version   10
    And Line should match alpine package version   ${lines[2]}   busybox
    And Line should match alpine package version   ${lines[3]}   alpine-baselayout
    And Line should match python package version   ${lines[4]}   robotframework
    And Line should match python package version   ${lines[5]}   robotframework-requests
    And Should Be Equal   ${lines[6]}   robot=${version_rf}|C
    And Should Be Equal   ${lines[7]}   pip=${version_pip}|C
    And Should Be Equal   ${lines[8]}   ${PACKAGE1_VERSION}|S
    And Should Be Equal   ${lines[9]}   ${PACKAGE2_VERSION}|S

[-d] option with missing argument
    [Tags]    get-local-versions    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${DOCKER_IMAGE_DIGEST}   -d
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-d] argument with default /etc value
    [Tags]    get-local-versions    root
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${DOCKER_IMAGE_DIGEST}   -d   /etc
    Then Should Be Equal As Integers    ${result.rc}    0
    And VERSIONS file should contain x lines and should match from and alpine version   2

[-d] argument with /tmp value
    [Tags]    get-local-versions    root
    Given Set Local Variable    ${VERSIONS_FILE}    /tmp/VERSIONS
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${DOCKER_IMAGE_DIGEST}   -d   /tmp
    Then Should Be Equal As Integers    ${result.rc}    0
    And VERSIONS file should contain x lines and should match from and alpine version   2

*** Keywords ***
VERSIONS file should contain x lines and should match from and alpine version
    [Arguments]    ${count}
    File Should Exist    ${VERSIONS_FILE}
    ${file_content} =   Get File    ${VERSIONS_FILE}
    ${line_count} =   Get Line Count   ${file_content}
    Should Be Equal As Integers   ${line_count}   ${count}
    @{lines} =   Split To Lines   ${file_content}
    Should Be Equal   ${lines[0]}   from=${DOCKER_IMAGE_DIGEST}|F
    Should Match Regexp   ${lines[1]}   ^alpine=${ALPINE_VERSION_PATTERN}|O$
    ${alpine_file} =   Get File    ${ALPINE_VERSION_FILE}
    @{alpine_file_lines} =   Split To Lines   ${alpine_file}
    Should Be Equal As Strings  ${lines[1]}   alpine=${alpine_file_lines[0]}|O
    [Return]  ${lines}

Line should match alpine package version
    [Arguments]    ${line}    ${package}
    ${package_info} =   When Run Process    apk update >/dev/null 2>&1 && apk info -d ${package} | head -n 1   shell=yes
    Should Be Equal As Integers    ${package_info.rc}    0
    ${matches} =   get regexp matches   ${package_info.stdout}   ${ALPINE_PACKAGE_PATTERN}
    Should Be Equal   ${line}   ${package}=${matches[0]}|A

Line should match python package version
    [Arguments]    ${line}    ${package}
    ${version}=   Get python package version   ${package}
    Should Be Equal   ${line}   ${package}=${version}|P

Get python package version
    [Arguments]    ${package}
    ${package_info} =   When Run Process    pip show ${package} | grep Version  shell=yes
    Should Be Equal As Integers    ${package_info.rc}    0
    ${matches} =   get regexp matches   ${package_info.stdout}   ${PYTHON_PACKAGE_PATTERN}
    [Return]  ${matches[0]}

** Settings ***
Documentation     get-tag-index.sh tests
...
...               A test suite to validate get-tag-index shell script.

Library           OperatingSystem
Library           Process
Library           String

*** Variables ***
${TESTS_DIR}   tests
${SHELL_DIR}   files
${SHELL.SH}   get-tag-index.sh
${USAGE_DISPLAY}   Usage: ${SHELL.SH} -r
${REPOSITORY}   myRepository
${USERNAME}   Username
${PASSWORD}   password
${TAG}   1.0.0

*** Test Cases ***
with no option
    [Tags]    get-tag-index    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -r argument is mandatory
    And Should Contain    ${result.stderr}    -u argument is mandatory
    And Should Contain    ${result.stderr}    -p argument is mandatory
    And Should Contain    ${result.stderr}    -t argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

with unknown option
    [Tags]    get-tag-index    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -x
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Illegal option -x
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-r] option missing
    [Tags]    get-tag-index    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -u   ${USERNAME}   -p   ${PASSWORD}   -t   ${TAG}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -r argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-r] option with missing argument
    [Tags]    get-tag-index    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   -u   ${USERNAME}   -p   ${PASSWORD}   -t   ${TAG}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-u] option missing
    [Tags]    get-tag-index    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -p   ${PASSWORD}   -t   ${TAG}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -u argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-u] option with missing argument
    [Tags]    get-tag-index    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   -p   ${PASSWORD}   -t   ${TAG}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-p] option missing
    [Tags]    get-tag-index    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -t   ${TAG}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -p argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-p] option with missing argument
    [Tags]    get-tag-index    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -p   -t   ${TAG}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-t] option missing
    [Tags]    get-tag-index    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -p   ${PASSWORD}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -t argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-t] option with missing argument
    [Tags]    get-tag-index    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -p   ${PASSWORD}   -t
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-v] option
    [Tags]    get-tag-index    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -v
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${SHELL.SH}, version

[--version] option
    [Tags]    get-tag-index    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   --version
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${SHELL.SH}, version

[-h] option
    [Tags]    get-tag-index    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -h
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

Wrong option username
    [Tags]    get-tag-index    dockerhub
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   wrongUsername   -p   ${PASSWORD}   -t   ${TAG}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Unable to login to Dockerhub, response code: 401

Wrong option password
    [Tags]    get-tag-index    dockerhub
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -p   wrongPassword   -t   ${TAG}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Unable to login to Dockerhub, response code: 401

Wrong option repository
    [Tags]    get-tag-index    dockerhub
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   wrongRepository   -u   ${USERNAME}   -p   ${PASSWORD}   -t   ${TAG}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Unable to get tags list for wrongRepository on Dockerhub, response code: 404

Not existing tag returns 1
    [Tags]    get-tag-index    dockerhub
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -p   ${PASSWORD}   -t   1.0.0
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Be Equal    ${result.stdout}    1

Continuous existing tag returns last + 1
    [Tags]    get-tag-index    dockerhub    sanity
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -p   ${PASSWORD}   -t   1.0.1
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Be Equal    ${result.stdout}    4

Missing first index of existing tag returns last + 1
    [Tags]    get-tag-index    dockerhub
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -p   ${PASSWORD}   -t   1.0.2
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Be Equal    ${result.stdout}    3

Discontinuous existing tag returns last + 1
    [Tags]    get-tag-index    dockerhub
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -p   ${PASSWORD}   -t   1.0.3
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Be Equal    ${result.stdout}    4

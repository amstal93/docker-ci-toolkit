** Settings ***
Documentation     sanity tests
...
...               Sanity simple test to check version.

Library           Process

*** Variables ***
${VERSION}   __VERSION__
${IMAGE}   myImage

*** Test Cases ***
[-v] option should show version: compare-versions.sh
    [Tags]    sanity-version
    Check version    compare-versions.sh

[-v] option should show version: get-identical-tag.sh
    [Tags]    sanity-version
    Check version    get-identical-tag.sh

[-v] option should show version: get-local-versions.sh
    [Tags]    sanity-version
    Check version    get-local-versions.sh

[-v] option should show version: get-tag-index.sh
    [Tags]    sanity-version
    Check version    get-tag-index.sh

[-v] option should show version: image-to-badge.sh
    [Tags]    sanity-version
    Check version    image-to-badge.sh

[-v] option should show version: readme-to-dockerhub.sh
    [Tags]    sanity-version
    Check version    readme-to-dockerhub.sh

[-v] option should show version: set-dockerhub-tag.sh
    [Tags]    sanity-version
    Check version    set-dockerhub-tag.sh

[-v] option should show version: set-dockerhub-tag-multiarch.sh
    [Tags]    sanity-version
    Check version    set-dockerhub-tag-multiarch.sh

[-v] option should show version: versions-to-readme.sh
    [Tags]    sanity-version
    Check version    versions-to-readme.sh

*** Keywords ***
Check version
    [Arguments]    ${script}
    ${result} =    When Run Process    docker    run    -t    --rm    ${IMAGE}    ${script}    -v
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    version ${VERSION}

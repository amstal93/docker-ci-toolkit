** Settings ***
Documentation     versions-to-readme.sh tests
...
...               A test suite to validate versions-to-readme shell script.

Library           OperatingSystem
Library           Process
Library           String

*** Variables ***
${TESTS_DIR}   tests
${SHELL_DIR}   files
${SHELL.SH}   versions-to-readme.sh
${USAGE_DISPLAY}   Usage: ${SHELL.SH} -r

*** Test Cases ***
with no option
    [Tags]    versions-to-readme    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -r argument is mandatory
    And Should Contain    ${result.stderr}    -f argument is mandatory
    And Should Contain    ${result.stderr}    -p argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

with unknown option
    [Tags]    versions-to-readme    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -x
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Illegal option -x
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-r] option missing
    [Tags]    versions-to-readme    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${TESTS_DIR}/files/version_f1_ref.txt   -p   See versions on
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -r argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-r] option with missing argument
    [Tags]    versions-to-readme    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   -f   ${TESTS_DIR}/files/version_f1_ref.txt   -p   See versions on
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-f] option missing
    [Tags]    versions-to-readme    simple
    Given Copy File   ${TESTS_DIR}/files/README_f1_ref.md   /tmp/README.md
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   /tmp/README.md   -p   See versions on
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -f argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-f] option with missing argument
    [Tags]    versions-to-readme    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   /tmp/README.md   -f   -p   See versions on
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-p] option missing
    [Tags]    versions-to-readme    simple
    Given Copy File   ${TESTS_DIR}/files/README_f1_ref.md   /tmp/README.md
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   /tmp/README.md   -f   ${TESTS_DIR}/files/version_f1_ref.txt
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -p argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-p] option with missing argument
    [Tags]    versions-to-readme    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   /tmp/README.md   -f   ${TESTS_DIR}/files/version_f1_ref.txt   -p
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-r] option argument with wrong file path
    [Tags]    versions-to-readme    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   /tmp/Wrong_README.md   -f   ${TESTS_DIR}/files/version_f1_ref.txt   -p   See versions on
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Error: readme file /tmp/Wrong_README.md does not exist!

[-f] option argument with wrong file path
    [Tags]    versions-to-readme    simple
    Given Copy File   ${TESTS_DIR}/files/README_f1_ref.md   /tmp/README.md
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   /tmp/README.md   -f   ${TESTS_DIR}/files/Wrong_version_f1_ref.txt   -p   See versions on
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Error: versions file
    And Should Contain    ${result.stderr}    Wrong_version_f1_ref.txt does not exist!

[-v] option
    [Tags]    versions-to-readme    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -v
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${SHELL.SH}, version

[--version] option
    [Tags]    versions-to-readme    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   --version
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${SHELL.SH}, version

[-h] option
    [Tags]    versions-to-readme    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -h
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-r] option argument with empty readme file
    [Tags]    versions-to-readme    simple
    Given Copy File   ${TESTS_DIR}/files/README_f1_empty.md   /tmp/README.md
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   /tmp/README.md   -f   ${TESTS_DIR}/files/version_f1_ref.txt   -p   See versions on
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Error: pattern See versions on does not exist in file /tmp/README.md!

[-f] option argument with empty versions file
    [Tags]    versions-to-readme    simple
    Given Copy File   ${TESTS_DIR}/files/README_f1_ref.md   /tmp/README.md
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   /tmp/README.md   -f   ${TESTS_DIR}/files/version_f6_empty.txt   -p   See versions on
    Then Should Be Equal As Integers    ${result.rc}    0
    ${file_content} =   Get File    /tmp/README.md
    And Should Contain    ${file_content}    | Software | Version |
    And Should Not Contain    ${file_content}    See versions on

[-f] option argument with wrong format versions file
    [Tags]    versions-to-readme    simple
    Given Copy File   ${TESTS_DIR}/files/README_f1_ref.md   /tmp/README.md
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   /tmp/README.md   -f   ${TESTS_DIR}/files/version_f6_empty.txt   -p   See versions on
    Then Should Be Equal As Integers    ${result.rc}    0
    ${file_content} =   Get File    /tmp/README.md
    And Should Contain    ${file_content}    | Software | Version |
    And Should Not Contain    ${file_content}    alpine
    And Should Not Contain    ${file_content}    See versions on

[-p] option argument with inexisting patern in readme file
    [Tags]    versions-to-readme    simple
    Given Copy File   ${TESTS_DIR}/files/README_f1_ref.md   /tmp/README.md
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   /tmp/README.md   -f   ${TESTS_DIR}/files/version_f1_ref.txt   -p   Wrong pattern
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Error: pattern Wrong pattern does not exist in file /tmp/README.md!

Successful usage
    [Tags]    versions-to-readme    simple    sanity
    Given Copy File   ${TESTS_DIR}/files/README_f1_ref.md   /tmp/README.md
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   /tmp/README.md   -f   ${TESTS_DIR}/files/version_f1_ref.txt   -p   See versions on
    Then Should Be Equal As Integers    ${result.rc}    0
    ${file_content} =   Get File    /tmp/README.md
    And Should Contain    ${file_content}    Docker latest tag contains:
    And Should Contain    ${file_content}    | Software | Version |
    And Should Contain    ${file_content}    from | docker@sha256:32a64c70 ...
    And Should Contain    ${file_content}    alpine | 3.10.1
    And Should Contain    ${file_content}    python3 | 3.7.3-r0
    And Should Contain    ${file_content}    ansible | 2.8.3
    And Should Contain    ${file_content}    ansible-lint | 4.1.0
    And Should Contain    ${file_content}    \## License
    And Should Not Contain    ${file_content}    See versions on
    Log    ${file_content}

